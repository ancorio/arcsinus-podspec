﻿Pod::Spec.new do |s|

  s.name = "arcsinus-artransitionkit"
  s.version = "0.3.0"
  s.summary = "UIKit extension"
  s.homepage = "https://bitbucket.org/ancorio/arcsinus-artransitionkit"
  s.license = { :type => "Custom", :text => "Copyright (c) 2017 Arcsinus LLC. All rights reserved." }
  s.authors = { "Erokhin Sergey" => "erokhin@arcsinus.ru" }
  s.ios.deployment_target = "9.0"
  s.source = { :git => "https://bitbucket.org/ancorio/arcsinus-artransitionkit.git", :tag => s.version.to_s }
  s.source_files = "ARTransitionKit/**/*.{swift,h,m}"
  s.public_header_files = "ARTransitionKit/**/*.h"
  s.resources = ['ARTransitionKit/**/*.xcassets', 'ARTransitionKit/**/*.xib']

end
